import React, { Component } from 'react';
import CalculatorComponent from './CalculatorComponent';
import { View } from 'react-native';


class Main extends Component {
    render(){
        return(
            <View style={{backgroundColor: 'lightpink'}}>
                <CalculatorComponent />
            </View>
        )
    }
}

export default Main;