import React from 'react';
import { Text, StyleSheet, View, Picker, ScrollView } from 'react-native';
import { Input, Button } from 'react-native-elements'


export class CalculatorComponent extends React.Component{
    constructor(prop){
        super(prop);
        this.state = {
            billPrices: 0,
            pickingPercentage: 0,
            otherPercentage: 0,
            totalTip: null,
            totalAmount: null
        };

        // this.handlePriceChange = this.handlePriceChange.bind(this);
        // this.handlePercentage = this.handlePercentage.bind(this);
        // this.handleOtherPercentage = this.handleOtherPercentage.bind(this);
        this.tipsCalculator = this.tipsCalculator.bind(this);
        
    };
    
    // handlePriceChange(e){
    //     var price = Number(e.target.value);
    //     this.setState ({
    //         billPrices: price
    //     })
    // }

    // handlePercentage(e){
    //     let percentage = null;
    //     if (e.target.value) {
    //         percentage = Number(e.target.value);
    //     }
    //     this.setState({
    //         pickingPercentage: percentage
    //     })
    // }

    // handleOtherPercentage(e){
    //     var other = Number(e.target.value);
    //     this.setState({
    //         otherPercentage: other
    //     })
    // }

    tipsCalculator(){
        //calculate
        if(this.state.billPrices === '' || this.state.pickingPercentage === 0){
            alert ('Please enter value');
            return;
        }
        
        let tip = 0;
        let total = 0;
        if (this.state.pickingPercentage === null){
            tip = this.state.billPrices * (this.state.otherPercentage/100);
        }
        else {
            tip = this.state.billPrices * Number(this.state.pickingPercentage);
        }

        //round 2 decimal
            tip = Math.round(tip*100)/100;
            total = Math.round((tip + this.state.billPrices)*100)/100;

        //update state
        this.setState({totalTip: tip});
        this.setState({totalAmount: total});
    }

    render(){
        let customStyle = {
            color: 'red'
        }
        return (
            <ScrollView>
                <Text style={styles.header}>Tip Calculator</Text>
                <View style = {styles.formRow}>
                    <Text style={styles.formLabel}>Prices</Text>
                    <Input
                        placeholder="Prices"
                        onChangeText={(value) => this.setState({billPrices: Number(value)})}
                        value = {this.state.billPrices}
                        containerStyle={styles.formItem}
                        />
                </View>
                <View style = {styles.formRow}>
                    <Text style={styles.formLabel}>Percentages</Text>
                    <Picker
                        style={styles.formItem}
                        selectedValue={this.state.pickingPercentage}
                        onValueChange={(itemValue, itemIndex) => this.setState({pickingPercentage: itemValue})}
                        >
                        <Picker.Item label='5 &#37; &#45; ok' value = '0.05'/>
                        <Picker.Item label='10 &#37; &#45; fine' value = '0.10'/>
                        <Picker.Item label='15 &#37; &#45; good' value = '0.15'/>
                        <Picker.Item label='20 &#37; &#45; excellent' value = '0.20'/>
                        <Picker.Item label='Other' value = {null} />
                    </Picker>
                    {/* <Input 
                        placeholder="Percentages"
                        onChangeText={(pickingPercentage) => this.setState({pickingPercentage})}
                        value={this.state.pickingPercentage}
                        containerStyle={styles.formInput}
                        /> */}
                </View>
                {this.state.pickingPercentage === null && (
                    <View style = {styles.formRow}>
                    <Text style={styles.formLabel}>Other Percentages</Text>
                    <Input
                        placeholder="Other Percentages"
                        onChangeText={(value) => this.setState({otherPercentage: Number(value)})}
                        value={this.state.otherPercentage}
                        containerStyle={styles.formItem}
                        />
                </View>
                )}
                <View style={styles.formRow}>
                    <Button
                        buttonStyle={styles.submitButton}
                        title="Submit"
                        onPress={() => this.tipsCalculator()}
                    />
                </View>
                <View style={styles.formRow}>
                    <Text style={styles.formLabel}>Total Tip</Text>
                    <Text style={styles.formItem}>{this.state.totalTip}</Text>
                </View>
                <View style={styles.formRow}>
                    <Text style={styles.formLabel}>Total Amount</Text>
                    <Text style={styles.formItem}>{this.state.totalAmount}</Text>
                </View>

                {/* <div className ='tip-calculator' style={{color: 'red'}}>
                    <h1 style={customStyle}>Tips Calculator</h1>
                    <form>
                        <div class = 'formGroup'> 
                            <label for ='prices'>Prices:</label>
                            <input type = 'text' id = 'prices' onChange = {this.handlePriceChange} placeholder= 'Prices'/>
                        </div>

                        <div class = 'formGroup'>
                            <label for = 'percentages'>Percentages:</label>
                            <select name ='percentages' onChange = {this.handlePercentage} id='tip'>
                                <option disabled selected value ='0'>--Choose an Option--</option>
                                <option value = '0.05'>5&#37; &#45; ok</option>
                                <option value = '0.10'>10&#37; &#45; fine</option>
                                <option value = '0.15'>15&#37; &#45; good</option>
                                <option value = '0.20'>20&#37; &#45; excellent</option>
                                <option value = ''>Other</option>  
                            </select>
                        </div>
                
                        <div class ='formGroup'>
                            <label for = 'other' >Please type other percentages:</label>
                            <input id = 'other' type = 'text'  onChange = {this.handleOtherPercentage} placeholder = 'Other Tips' /> 
                        </div>
            
                        <button id = 'amount' onClick = {this.tipsCalculator} type = 'button'>Submit</button> 

                        {this.state.totalTip && (
                            <>
                                <div id = 'tipAmount' class = 'total'>
                                    <sup>$</sup><span id="totalTips">{this.state.totalTip}</span>
                                </div>

                                <div id = 'totalAmount' class = 'total'>
                                    <sup>$</sup><span id="amountTotal">{this.state.totalAmount}</span>
                                </div>
                            </>
                        )}
                    </form>
                    <footer>
                        <h5>by Hien Ho</h5>
                    </footer>
                </div>    */}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    formRow: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'row',
        margin: 1
    },
    formLabel: {
        fontSize: 18,
        flex: 2
    },
    formItem: {
        flex: 1
    },
    header: {
        fontSize: 20,
        color: 'red',
        textAlign: 'center',
        fontWeight: 'bold'
    },
    submitButton: {
        backgroundColor: '#512DA8'
    },
})

export default CalculatorComponent;